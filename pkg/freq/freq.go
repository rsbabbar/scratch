package main

import (
	"errors"
	"unicode/utf8"
)

// Frequency calculates the character frequency for a string.
func Frequency(s string) map[rune]int {
	freq := make(map[rune]int)
	for _, c := range s {
		freq[c]++
	}
	return freq
}

// MinOccuring returns the least frequent character in the given input string.
func MinOccuring(s string) (rune, error) {
	if s == "" {
		return '0', errors.New("minOccuring: no minimum, string is empty")
	}

	var (
		freq       = Frequency(s)
		min        = len(s) + 1                 // initial min occurrences
		minRune, _ = utf8.DecodeRuneInString(s) // initial minimum value
	)
	for k, v := range freq {
		if v < min {
			min = v
			minRune = k
		}
	}
	return minRune, nil
}

// MaxOccuring returns the most frequent character in the given input string.
func MaxOccuring(s string) (rune, error) {
	if s == "" {
		return '0', errors.New("maxOccuring: no maximum, string is empty")
	}

	var (
		freq       = make(map[rune]int)
		max        = 0                          // initial max occurrences
		maxRune, _ = utf8.DecodeRuneInString(s) // initial maximum value
	)
	for _, c := range s {
		freq[c]++

		count := freq[c]
		if count > max {
			max = count
			maxRune = c
		}
	}
	return maxRune, nil
}
