package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

func main() {
	var (
		sb strings.Builder
		// nolint:gosec // not used for crypto
		r = rand.New(rand.NewSource(time.Now().UnixNano()))
	)
	const (
		min, max = 140, 180
		count    = 28
	)
	for i := 0; i < count; i++ {
		n := r.Intn(max-min+1) + min
		sb.WriteString(fmt.Sprintln(n))
	}
	fmt.Print(sb.String())
}
